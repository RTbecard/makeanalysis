require(R6)

file_script <- R6::R6Class(
  classname = "file_script",
  private = list(
    .path = NULL,
    .depends = list(),
    .returns = list()
  ),
  public = list(
    print = function(){
      cat("MakeAnalysis script file\n")
      cat(sprintf("path: %s\n", private$.path))
      cat("\nDepends on:\n")
      cat(sprintf(" - %s\n", sapply(private$.depends, function(x) x$get_path())), sep = "")
      cat("\nReturns:\n")
      cat(sprintf(" - %s\n", sapply(private$.returns, function(x) x$get_path())), sep = "")

    },
    set_path = function(path) .set_path(path, private, check = T),
    get_path = function() private$.path,
    initialize = function(path){
      # Set path
      self$set_path(path)
    },
    get_depends = function(x) private$.depends,
    add_dependency = function(x){
      .check_type(x, "file_data")
      .check_already_in_list(private$.depends, x, "%s is already listed as a dependent file.")
      private$.depends[[length(private$.depends) + 1 ]] <- x
    },
    remove_dependency = function(x){
      .check_type(x, "file_data")
      idx <- .find_in_list(private$.depends, x, "%s is not in the dependency list.")
      private$.depends <- private$.depends[-idx]
    },
    get_returns = function(x) private$.returns,
    add_return = function(x){
      .check_type(x, "file_data")
      .check_already_in_list(private$.returns, x, "%s is already listed as return file")
      private$.returns[[length(private$.returns) + 1]] <- x
    },
    remove_return = function(x){
      .check_type(x, "file_data")
      idx <- .find_in_list(private$.returns, x, "%s is not in the returns list.")
      private$.returns <- private$.returns[-idx]
    }
  ))

file_data <- R6::R6Class(
  classname = "file_data",
  private = list(
    .path = NULL,
    .type = NULL,
    .temporary = F
  ),
  public = list(
    print = function(){
      cat("MakeAnalysis data file\n")
      cat(sprintf("path: %s\n", private$.path))
      cat(sprintf("type: %s\n", private$.type))
    },
    set_path = function(path) .set_path(path, private, check = !private$.temporary),
    get_path = function() private$.path,
    get_type = function() private$.type,
    initialize = function(path, type){
      .check_type(type, 'character')
      tbl <- c("input", "temporary", "result")
      mtch <- pmatch(type[1], tbl)
      if(is.na(mtch)){
        stop("`type` must be one of the character strings {'input', 'temporary', 'result'}.")
      }else{
        private$.type = tbl[mtch]
      }
      if(private$.type == "temporary")
        private$.temporary = T

      # Set path
      self$set_path(path)
    }
  )
)
